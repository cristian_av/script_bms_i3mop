import math
from random import randint
import re
from decimal import Decimal
from datetime import date
from sqlalchemy.engine import create_engine
import xlsxwriter
import xlrd

# Listado de puentes del catastro a realizar
catastro_puentes = list() 
# Listado de puentes que ya existen en i3MOP y no hay que considerar en la carga.
excluir_puentes = list()
# Puentes BMS que no fueron cargados al excel.
bms_excluidos = list()


def conectar_mysql(host, user, passwd, db):
    engine = create_engine(
         'mysql://{}:{}@{}:3306/{}?charset=utf8'.format(
             user, passwd, host, db))
    return engine


def obtener_puentes_bms(db_bms):
    """
    Función que se conecta a la Bd MySql del sistema I3MOP y obtiene los puentes a migrar.
    Arguments:
        db_bms {Conexión SqlAlchemy} -- Conexión a la base de datos Mysql del sistema BMS.
    
    Returns:
        Lista -- Listado de puentes del sistema BMS que cumplen con los criterios para 
        ser migrados al sistema I3MOP.
    """

    puentes_bms = db_bms.execute(
        """
        SELECT a.codigo, a.nombre, b.rol, c.nombre as 'comuna', d.nombre as 'provincia', a.kilometro,
        a.largo, a.ancho_total, e.nombre as 'subtipo', f.nombre as 'tipo', a.comentario_puente,
        h.nombre as 'cauce', a.latitud, a.longitud, a.anio_construccion, i.esviaje,i.comentario_super,
        (SELECT j.comentario_insp from Inspeccion j WHERE j.codigo_puente = a.codigo 
        ORDER BY fecha_inspeccion DESC LIMIT 1 ) AS 'comentario_insp'
        FROM uachbms.Puente a LEFT OUTER JOIN Ruta b ON (a.idRuta = b.idRuta)
        LEFT OUTER JOIN Comuna c ON (a.idComuna = c.idComuna)
        LEFT OUTER JOIN Provincia d ON (c.idProvincia = d.idProvincia)
        LEFT OUTER JOIN SubtipoPuente e ON (a.idSubTipo = e.idSubtipo)
        LEFT OUTER JOIN TipoPuente f ON (e.idTipo = f.idTipo)
        LEFT OUTER JOIN CaucePuente g ON (g.codigo_puente = a.codigo)
        LEFT OUTER JOIN Cauce h ON (g.idCauce = h.idCauce)
        LEFT OUTER JOIN Superestructura i ON (a.idSuperestructura = i.idSuperestructura)
		ORDER BY a.codigo ASC
        """)
    puentes = list()
    for row in puentes_bms:
        puentes.append({
            'nombre': row.nombre,
            'rol': row.rol,
            'comuna': row.comuna,
            'provincia': row.provincia,
            'kilometro': row.kilometro,
            'largo': row.largo,
            'ancho_total': row.ancho_total,
            'subtipo': row.subtipo,
            'tipo': row.tipo,
            'comentario_puente': row.comentario_puente,
            'cauce': row.cauce,
            'latitud': row.latitud,
            'longitud': row.longitud,
            'anio_construccion': row.anio_construccion,
            'esviaje': row.esviaje,
            'comentario_super': row.comentario_super,
            'comentario_insp': row.comentario_insp,
            })
    return puentes


def plantilla_excel():
    """ Función que crea el archivo excel con la estructura I3MOP entregada.
    Se crean las columnas y los códigos de cabeceras correspondientes.
    Returns:
        Objeto Excel -- Objeto excel con las hojas y columnas para migrar
        al sistema I3MOP.
    """
    # Creación del archivo
    excel = xlsxwriter.Workbook('test.xlsx')
    # creación de hojas
    excel.add_worksheet('Ayuda')
    datos_generales = excel.add_worksheet('DATOS_GENERALES')
    # Formato
    bold = excel.add_format({'bold': True})
    # Creación de colunas hoja DATOS_GENERALES
    datos_generales.write(0, 0, '62c110a5-db42-43c5-9120-df52df9bc74c')
    datos_generales.write(1, 0, 'Datos Generales - Código i3met', bold)
    datos_generales.write(0, 1, '465a9ea0-3259-0131-817b-0c89a5536ba7')
    datos_generales.write(1, 1, 'Nombre', bold)
    datos_generales.write(0, 2, 'ad3fd671-d855-4991-b66e-eae44e599409')
    datos_generales.write(1, 2, 'Código', bold)
    datos_generales.write(0, 3, 'f214a95c-9739-47d1-a5c9-b557bf581681')
    datos_generales.write(1, 3, 'Comuna', bold)
    datos_generales.write(0, 4, '2d24ceef-e246-4fc5-990e-2c8be4e1b53d')
    datos_generales.write(1, 4, 'Provincia', bold)
    datos_generales.write(0, 5, '6261290c-4987-4c3c-9624-98ae7d1461bb')
    datos_generales.write(1, 5, 'Region', bold)
    datos_generales.write(0, 6, 'b220bded-46d8-4c4f-89c0-0ca695824e98')
    datos_generales.write(1, 6, 'Mandante', bold)
    datos_generales.write(0, 7, '465aa6c0-3259-0131-817b-0c89a5536ba7')
    datos_generales.write(1, 7, 'Ruta', bold)
    datos_generales.write(0, 8, '87c57dab-aab8-4398-a87c-72d73c8901de')
    datos_generales.write(1, 8, 'Clase de ruta', bold)
    datos_generales.write(0, 9, '465aa7e0-3259-0131-817b-0c89a5536ba7')
    datos_generales.write(1, 9, 'DM', bold)
    datos_generales.write(0, 10, 'ff9e046b-0727-431a-86d1-a80406af1b69')
    datos_generales.write(1, 10, 'Tipología General', bold)
    datos_generales.write(0, 11, 'd407a68f-8ac4-450d-967d-5f69ee85a7da')
    datos_generales.write(1, 11, 'Tipologia estructural', bold)
    datos_generales.write(0, 12, '465aab40-3259-0131-817b-0c89a5536ba7')
    datos_generales.write(1, 12, 'Longitud total (m)', bold)
    datos_generales.write(0, 13, '2fea6d02-4adc-427f-bffb-429cac56aec4')
    datos_generales.write(1, 13, 'Ancho total (m)', bold)
    datos_generales.write(0, 14, '86dc9b5d-187d-49f8-a2a9-9c3bb64c942c')
    datos_generales.write(1, 14, 'Superficie total (m2)', bold)
    datos_generales.write(0, 15, 'fb68c4c1-2853-4e66-bc74-8504ffbc6caa')
    datos_generales.write(1, 15, 'Sentido de transito', bold)
    datos_generales.write(0, 16, '465aa210-3259-0131-817b-0c89a5536ba7')
    datos_generales.write(1, 16, 'Fecha de inventario', bold)
    datos_generales.write(0, 17, '89231b11-9537-447d-bf21-c455990d9fb2')
    datos_generales.write(1, 17, 'Importancia', bold)
    datos_generales.write(0, 18, '2277f67e-1d50-43d4-9200-01c6be7a9c9a')
    datos_generales.write(1, 18, 'Activa', bold)
    datos_generales.write(0, 19, '7cc17beb-caba-4ec5-8152-faf475fe11ba')
    datos_generales.write(1, 19, 'Operatividad', bold)
    datos_generales.write(0, 20, 'a672f156-dd7a-46e7-89d3-63bf184a398f')
    datos_generales.write(1, 20, 'Clase de exposición ambiental', bold)
    datos_generales.write(0, 21, '465a9fd0-3259-0131-817b-0c89a5536ba7')
    datos_generales.write(1, 21, 'Profesional de catastro', bold)
    datos_generales.write(0, 22, '87079a46-3fc3-4931-8528-86ef23b563d5')
    datos_generales.write(1, 22, 'Supervisor', bold)
    datos_generales.write(0, 23, 'cf43b6af-f2bc-43d3-a429-9240480e9d1e')
    datos_generales.write(1, 23, 'Descripcion de la Estructura', bold)
    datos_generales.write(0, 24, '42a6fedd-0bda-4027-8b17-440eb654710b')
    datos_generales.write(1, 24, 'Geometría', bold)
    datos_generales.write(0, 25, '5be4ce78-f7a0-4c5e-8251-92fbb0d4cd98')
    datos_generales.write(1, 25, 'Uso permitido - Vehículos', bold)
    datos_generales.write(0, 26, 'fadf6518-ba27-46e1-8148-1e8e7856ee59')
    datos_generales.write(1, 26, 'Uso permitido - FF.CC.', bold)
    datos_generales.write(0, 27, 'f82d5751-fd14-4d51-acf4-0f4c063ab5c2')
    datos_generales.write(1, 27, 'Uso permitido - Peatones', bold)
    datos_generales.write(0, 28, 'c38ab4b3-73d6-4907-9f92-1c8ea02e8695')
    datos_generales.write(1, 28, 'Uso permitido - Bicicletas', bold)
    datos_generales.write(0, 29, 'cdba3bb6-a6de-4094-bd7f-d837d05c16a6')
    datos_generales.write(1, 29, 'Uso permitido - Otros', bold)
    datos_generales.write(0, 30, '098a7f35-dbcd-4427-9d51-42a33c436a4c')
    datos_generales.write(1, 30, 'Obstáculo salvado - Vehículos', bold)
    datos_generales.write(0, 31, '787bde0d-17cd-4aa1-b5d6-8ff3c194c76c')
    datos_generales.write(1, 31, 'Obstáculo salvado - FF.CC.', bold)
    datos_generales.write(0, 32, '04331ecc-0012-41d0-8a74-12d49ae9dc43')
    datos_generales.write(1, 32, 'Obstáculo salvado - Peatones', bold)
    datos_generales.write(0, 33, 'e54eff64-b48b-45f5-8352-4bf02570dabe')
    datos_generales.write(1, 33, 'Obstáculo salvado - Bicicletas', bold)
    datos_generales.write(0, 34, 'a11487df-5aec-41a8-9f97-4eb4b94996d5')
    datos_generales.write(1, 34, 'Obstáculo salvado - Cauce (nat/artif)', bold)
    datos_generales.write(0, 35, '90f133b0-e19e-43ac-b67d-fbb9285f8b39')
    datos_generales.write(1, 35, 'Obstáculo salvado - Irregularidad del terreno', bold)
    datos_generales.write(0, 36, '52921475-e7c8-499c-894a-4a08de6e606d')
    datos_generales.write(1, 36, 'Obstáculo salvado - Otros', bold)
    datos_generales.write(0, 37, '747a6060-3024-0131-8fa3-0c89a5536ba7')
    datos_generales.write(1, 37, 'Rutas / Calles - Soportada', bold)
    datos_generales.write(0, 38, '063b601b-b86c-4bd9-b569-17bd34a696d5')
    datos_generales.write(1, 38, 'Coordenadas - Altitud', bold)
    datos_generales.write(0, 39, '464e2176-56f7-490e-b003-4d31d49cd247')
    datos_generales.write(1, 39, 'Coordenadas - Lat', bold)
    datos_generales.write(0, 40, '4dfcf909-439e-4886-971b-a617d5def8b0')
    datos_generales.write(1, 40, 'Coordenadas - Long', bold)
    datos_generales.write(0, 41, '747a6480-3024-0131-8fa3-0c89a5536ba7')
    datos_generales.write(1, 41, 'Cronología - Construcción (Año)', bold)
    datos_generales.write(0, 42, '298c64b1-2ce5-4e09-921d-c28d935813aa')
    datos_generales.write(1, 42, 'Cronología - Nombre proyectista', bold)
    datos_generales.write(0, 43, '161ed141-0bf3-43e8-8fce-3d4dd6262b26')
    datos_generales.write(1, 43, 'Cronología - Nombre constructor', bold)
    datos_generales.write(0, 44, '465abd90-3259-0131-817b-0c89a5536ba7')
    datos_generales.write(1, 44, 'Cronología - Normativa técnica', bold)
    datos_generales.write(0, 45, '857a9fb3-06d9-4447-abaa-2b0374355882')
    datos_generales.write(1, 45, 'Cronología - Carga de diseño', bold)
    datos_generales.write(0, 46, '747a6580-3024-0131-8fa3-0c89a5536ba7')
    datos_generales.write(1, 46, 'Cronología - Última rehab. (Año)', bold)
    datos_generales.write(0, 47, 'aae531bb-556e-43e5-9f45-9ad640e5991c')
    datos_generales.write(1, 47, 'Cronología - Nombre proyectista', bold)
    datos_generales.write(0, 48, 'c74306ce-33b4-412f-8c1f-6d4504756377')
    datos_generales.write(1, 48, 'Cronología - Nombre constructor', bold)
    datos_generales.write(0, 49, '52dd1321-8669-4f3d-877a-001b29e39fac')
    datos_generales.write(1, 49, 'Cronología - Normativa técnica', bold)
    datos_generales.write(0, 50, '6542eaa4-e994-4d71-a094-229480bc1436')
    datos_generales.write(1, 50, 'Cronología - Carga de diseño', bold)
    datos_generales.write(0, 51, '747a66d0-3024-0131-8fa3-0c89a5536ba7')
    datos_generales.write(1, 51, 'Geometría - Trazado en planta', bold)
    datos_generales.write(0, 52, '8ef300eb-43cc-4c0b-bdc2-4039b56aa26c')
    datos_generales.write(1, 52, 'Geometría - Ángulo de esviaje(º)', bold)
    datos_generales.write(0, 53, 'a0dfa7cb-15db-4097-b59f-2e7159c0bff1')
    datos_generales.write(1, 53, 'Geometría - Trazado en elevación', bold)
    datos_generales.write(0, 54, 'cb51bf17-1393-4a6d-80ad-c5e23dee1341')
    datos_generales.write(1, 54, 'Geometría - k de proyecto', bold)
    datos_generales.write(0, 55, '747a6830-3024-0131-8fa3-0c89a5536ba7')
    datos_generales.write(1, 55, 'Cauce - Cauce', bold)
    datos_generales.write(0, 56, '747a6980-3024-0131-8fa3-0c89a5536ba7')
    datos_generales.write(1, 56, 'Observaciones - Observaciones', bold)
    return excel


def codigo_i3met(puente):
    """
    Función que recibe un objeto puente del sistema BMS y genera
    su Código I3MET correspondiente según la siguiente especificación.
    RRRR-CC-CCCC-DDDD-YY
    Región, camino, distancia, tipo. 
    
    Arguments:
        puente {Objeto SqlAlchemy} -- Fila correspondiente a un puente con toda la información
        en el sistema BMS según la query ejecutada en la función obtener_puentes_bms.
    
    Returns:
        String -- Código I3MET.
    """
    # RRRR: Código región de los Ríos.
    rrrr = '0074'  
    # CC-CCCC: Código de la carretera
    # Ej: D-55 = 0D-0055
    rol = (puente['rol'] or '').split('-', 1)  # Separamos el rol solo por el primer guión
    rol1 = rol[0].zfill(2).replace(' ', '')
    try:
        rol2 = rol[1].replace('-', '')
        rol2 = rol2.zfill(4)
    except:
        rol2 = '0000'
    cc_cccc = rol1 + '-' + rol2
    # DDDD: Distancia métrica en metros. (En el excel viene en kms y no están complentado con ceros a la derecha)
    # Ej: 10,06 = 0010,060
    dm = str(puente['kilometro']).split('.')
    try:
        decimal = "." + str(dm[1])
    except:
        decimal = ""
    dddd = dm[0].zfill(4) + str(decimal)
    # YY: Tipo de obra
    yy = '03'  # Puente
    return rrrr + '-' + cc_cccc + '-' + dddd + '-' + yy


def superficie_total(puente):
    """
    Función que calcula la superficie total de un puente del Sistema BMS.
    
    Arguments:
        puente {Objeto SqlAlchemy} -- Puente del sistema BMS.
    
    Returns:
        String -- Superficie total según el largo y ancho del puente en el
        sistema BMS.
    """
    try:
        largo = str(puente['largo'])
        ancho_total = str(puente['ancho_total'])
        return str(Decimal(largo) * Decimal(ancho_total))
    except:
        return ''


def fecha_inventario(puente):
    """Función que retorna la fecha de invetario del puente
    
    Arguments:
        puente {Objeto SqlAlchemy} -- Puente del sistema BMS a migrar.
    
    Returns:
        [String] -- Se retorna la fecha del día en que se ejecutó la migración.
    """
    hoy = date.today()
    return "{}/{}/{}".format(hoy.day, hoy.month, hoy.year)


def descripcion_estructura(puente):
    """Función que retorna la descripción de la estructura al combinar los
    campos tipo, subtipo, comentario_puente y comentario_super de un puente
    del sistema BMS.
    
    Arguments:
        puente {Objeto SqlAlchemy} -- Puente del sistema BMS a migrar.
    
    Returns:
        String -- Descripción de la estructura.
    """
    return "{} {} {} {}".format(
        puente['tipo'] or '',
        puente['subtipo'] or '',
        puente['comentario_puente'] or '',
        puente['comentario_super'] or '')


def obstaculo_salvado_cauce(puente):
    """Retorna si el puente tiene un obstáculo salvado cauce
    Arguments:
        puente {Objeto SqlAlchemy} -- Puente del sistema BMS a migrar.
    
    Returns:
        String -- Si o No.
    """
    if puente['cauce'] is not None:
        return 'Si'
    else:
        return 'No'


def tipologia_estructural(puente):
    """Retorna la tipología estructura según la conversión BMS-> I3MOP definida.
    
    Arguments:
        puente {Objeto SqlAlchemy} -- Puente del sistema BMS a migrar.
    
    Returns:
        String -- Tipología del puente según definición I3MOP.
    """
    tipologia = ''
    subtipo = puente['subtipo'] or ''
    if subtipo.find('celosí­a') > -1:
        tipologia = 'Convencional celosía'
    elif subtipo.find('cajón') > -1:
        tipologia = 'Convencional cajón'
    elif subtipo.find('Losa') > -1:
        tipologia = 'Convencional losa'
    elif subtipo.find('Marco') > -1:
        tipologia = 'Marco / Cajón / Pórtico'
    elif subtipo.find('Arco') > -1:
        tipologia = 'Arco'
    elif subtipo.find('colgante') > -1:
        tipologia = 'Colgante'
    elif subtipo.find('Viga') > -1:
        tipologia = 'Convencional vigas'
    return tipologia


def catastro_str1245(archivo='catastro_puentes.xlsx'):
    """Función que lee el archivo excel con el catastro de puentes
    que hay que inspeccionar en el proyecto STR-1245 y genera una lista
    disponible para otras funciones.
    
    Keyword Arguments:
        archivo {str} -- Archivo que contiene el catastro de puentes (default: {'catastro_puentes.xlsx'})
    
    Returns:
        List() -- Lista con los puentes a inspeccionar.
    """
    xl_workbook = xlrd.open_workbook(archivo)
    xl_sheet = xl_workbook.sheet_by_name('kmz2')
    global catastro_puentes

    catastro_puentes = list()
    for row_idx in range(1, xl_sheet.nrows):

        catastro_puentes.append({
            'nombre_activo': xl_sheet.cell(row_idx, 1).value,
            'rol_camino': xl_sheet.cell(row_idx, 2).value,
            'km_inicio': float(xl_sheet.cell(row_idx, 4).value) / 1000,  # Está en metros y en BD está en KMs.
            'comuna': xl_sheet.cell(row_idx, 5).value,
            'latitud': xl_sheet.cell(row_idx, 6).value,
            'longitud': xl_sheet.cell(row_idx, 7).value,
            'equivalencias': 0,
            'equivalentes': '',
        })
    return catastro_puentes


def catastro_i3mop(archivo='excluir_puentes.xlsx'):
    """Función que lee el archivo excel de puentes de la Región de los Ríos que ya están ingresados
    al sistema I3MOP.
    
    Keyword Arguments:
        archivo {str} -- Catastro de puentes en excel (default: {'excluir_puentes.xlsx'})
    
    Returns:
        List() -- Lista de puentes que ya están en sistema I3MOP.
    """
    xl_workbook = xlrd.open_workbook(archivo)
    xl_sheet = xl_workbook.sheet_by_name('excluir')
    global excluir_puentes
    excluir_puentes = list()
    for row_idx in range(1, xl_sheet.nrows):

        excluir_puentes.append({
            'nombre_activo': xl_sheet.cell(row_idx, 2).value,
            'rol_camino': xl_sheet.cell(row_idx, 8).value,
            'km_inicio': float(xl_sheet.cell(row_idx, 10).value),
            'comuna': xl_sheet.cell(row_idx, 4).value,
            'latitud': xl_sheet.cell(row_idx, 41).value, # OJO: viene invertidas la lat y long.
            'longitud': xl_sheet.cell(row_idx, 40).value,
            'equivalencias': 0,
            'equivalentes': '',
        })
    return excluir_puentes


def simplificar_ruta(texto):
    """Función que simplifica el Rol de una Ruta para facilitar la comparación entre los roles del sistema
    BMS y el sistema I3MOP. Se eliminan todos los caracteres que no son numéricos.
    
    Arguments:
        texto {String} -- Rol de la Ruta en sistema BMS.
    
    Returns:
        Número Entero -- Rol simplificado.
    """
    # Convertimos la ruta a solo un número
    texto = re.sub("[^0-9]", "", texto or '')
    return int(texto or randint(0,10000))


def equivalencia_str1245(catastro, puente):
    """Función que analiza si un puente del sistema BMS es equivalente a un puente del catastro STR-1245 
    del sistema I3MOP cargado en la función catastro_str1245.
    
    Arguments:
        catastro {Lista} -- Lista con el catastro de puentes a inspeccionar en proyecto STR-1245.
        puente {Objeto SqlAlchemy} -- Puente en el sistema BMS.
    
    Returns:
        Booleano -- Verdadero si se encuentra una equivalencia, falso si no.
    """
    equivalencias = list()
    for p in catastro:
        en_catastro = False
        # if p['nombre_activo'].casefold().replace('puente', '').strip() == puente['nombre'].casefold().replace('puente', '').strip():
        #     print(p['nombre_activo'], puente['nombre'], simplificar_ruta(p['rol_camino']), simplificar_ruta(puente['rol']), float(p['km_inicio']), float(puente['kilometro']))
        
        if simplificar_ruta(p['rol_camino']) == simplificar_ruta(puente['rol']) and abs(float(p['km_inicio'] or 0) - float(puente['kilometro'] or 0)) < 0.5:
            en_catastro = True
        elif abs(float(p['latitud']) - float(puente['latitud'])) < 0.001 and abs(float(p['longitud']) - float(puente['longitud'])) < 0.001:
            en_catastro = True

        if en_catastro:
            p['equivalencias'] += 1
            p['equivalentes'] += puente['nombre'] + " *** "
            equivalencias.append(p)
    return len(equivalencias) > 0


def resumen_filtros():
    """Función que genera un archivo excel con el resumen de los filtros aplicados como se resumen a continuación.
    1) catastro_str1245: Listado completo de los puentes a inspeccionar en el proyecto,
    destacando en amarillo los puentes que no tienen un equivalente en BMS.
    2) catastro_i3mop: Listado de puentes que actualmente están en I3MOP y que no deben
    ser incluídos en la migración, destacando en amarillo los puentes que no tienen un equivalente en BMS.
    3) excluidos_bms: Todos los puentes del sistema BMS que no fueron considerados en la migración por no
    estar en el catastro str1245, por que ya están en I3MOP o por que no tenían suficiente información
    (DM y ruta) para ser considerados.
    """
    global catastro_puentes
    global excluir_puentes
    global bms_excluidos

    # Creación del archivo
    excel = xlsxwriter.Workbook('resumen_filtros.xlsx')
    # creación de hojas
    hoja_str1245 = excel.add_worksheet('catastro_str1245')
    hoja_i3mop = excel.add_worksheet('catastro_i3mop')
    hoja_bms = excel.add_worksheet('excluidos_bms')
    bold = excel.add_format({'bold': True})
    yellow = excel.add_format({'bg_color': '#FFFF00'})

    hoja_str1245.write(0, 0, 'Nombre', bold)
    hoja_str1245.write(0, 1, 'Rol Ruta', bold)
    hoja_str1245.write(0, 2, 'DM', bold)
    hoja_str1245.write(0, 3, 'Comuna', bold)
    hoja_str1245.write(0, 4, 'Latitud', bold)
    hoja_str1245.write(0, 5, 'Longitud', bold)
    hoja_str1245.write(0, 6, 'Equivalencias', bold)
    hoja_str1245.write(0, 7, 'Equivalentes', bold)
    hoja_i3mop.write(0, 0, 'Nombre', bold)
    hoja_i3mop.write(0, 1, 'Rol Ruta', bold)
    hoja_i3mop.write(0, 2, 'DM', bold)
    hoja_i3mop.write(0, 3, 'Comuna', bold)
    hoja_i3mop.write(0, 4, 'Latitud', bold)
    hoja_i3mop.write(0, 5, 'Longitud', bold)
    hoja_i3mop.write(0, 6, 'Equivalencias', bold)
    hoja_i3mop.write(0, 7, 'Equivalentes', bold)
    hoja_bms.write(0, 0, 'Nombre', bold)
    hoja_bms.write(0, 1, 'Rol Ruta', bold)
    hoja_bms.write(0, 2, 'DM', bold)
    hoja_bms.write(0, 3, 'Comuna', bold)
    hoja_bms.write(0, 4, 'Latitud', bold)
    hoja_bms.write(0, 5, 'Longitud', bold)

    row = 1
    for p in catastro_puentes:
        formato = excel.add_format()
        if p['equivalencias'] < 1:
            formato = yellow
        hoja_str1245.write(row, 0, p['nombre_activo'], formato)
        hoja_str1245.write(row, 1, p['rol_camino'], formato)
        hoja_str1245.write(row, 2, p['km_inicio'], formato)
        hoja_str1245.write(row, 3, p['comuna'], formato)
        hoja_str1245.write(row, 4, p['latitud'], formato)
        hoja_str1245.write(row, 5, p['longitud'], formato)
        hoja_str1245.write(row, 6, p['equivalencias'], formato)
        hoja_str1245.write(row, 7, p['equivalentes'], formato)
        row += 1
    
    row = 1
    for p in excluir_puentes:
        formato = excel.add_format()
        if p['equivalencias'] < 1:
            formato = yellow
        hoja_i3mop.write(row, 0, p['nombre_activo'], formato)
        hoja_i3mop.write(row, 1, p['rol_camino'], formato)
        hoja_i3mop.write(row, 2, p['km_inicio'], formato)
        hoja_i3mop.write(row, 3, p['comuna'], formato)
        hoja_i3mop.write(row, 4, p['latitud'], formato)
        hoja_i3mop.write(row, 5, p['longitud'], formato)
        hoja_i3mop.write(row, 6, p['equivalencias'], formato)
        hoja_i3mop.write(row, 7, p['equivalentes'], formato)
        row += 1

    row = 1
    for p in bms_excluidos:
        hoja_bms.write(row, 0, p['nombre'])
        hoja_bms.write(row, 1, p['rol'])
        hoja_bms.write(row, 2, p['kilometro'] or '')
        hoja_bms.write(row, 3, p['comuna'])
        hoja_bms.write(row, 4, p['latitud'])
        hoja_bms.write(row, 5, p['longitud'])
        row += 1
    excel.close()


def crear_excel(excel, puentes):
    """Función que crea un archivo excel con las puentes del sistema BMS que hay que migrar al sistema
    I3MOP según la planilla excel entregada.
    
    Arguments:
        excel {Objeto} -- Objeto excel con la plantilla base (Hojas y columnas) para migrar al sistema I3MOP.
        puentes {Lista} -- Lista de puentes del sistema BMS que hay que migrar al sistema I3MOP.
    """
    # Create a workbook and add a worksheet.
    datos_generales = excel.get_worksheet_by_name('DATOS_GENERALES')

    global catastro_puentes
    global excluir_puentes
    global bms_excluidos
    
    catastro_str1245()
    catastro_i3mop()
    
    row = 2  # Comenzamos después de los headers.
    for puente in puentes:
        # Criterios de exclusión
        en_str1245 =  equivalencia_str1245(catastro_puentes, puente)
        en_i3mop = equivalencia_str1245(excluir_puentes, puente)
        if puente['kilometro'] is not None and en_str1245 and not en_i3mop:
            datos_generales.write(row, 0, codigo_i3met(puente))
            datos_generales.write(row, 1, puente['nombre'])
            # 2 Falta Código MOP
            datos_generales.write(row, 3, puente['comuna'])
            datos_generales.write(row, 4, puente['provincia'])
            datos_generales.write(row, 5, 'XIV Los Ríos (74)')
            datos_generales.write(row, 6, 'Dirección de Vialidad')
            datos_generales.write(row, 7, puente['rol'])
            # 8 Falta Clase camino 
            datos_generales.write(row, 9, str(puente['kilometro'] or ''))  # Convertimos a string para que quede con punto. 34.4
            datos_generales.write(row, 10, 'Puente')
            datos_generales.write(row, 11, tipologia_estructural(puente))
            datos_generales.write(row, 12, str(puente['largo'] or ''))
            datos_generales.write(row, 13, str(puente['ancho_total'] or ''))
            datos_generales.write(row, 14, superficie_total(puente))
            datos_generales.write(row, 15, 'Ambas')
            datos_generales.write(row, 16, fecha_inventario(puente))
            # 17 Falta importancia, preguntar al MOP
            datos_generales.write(row, 18, 'Sí')
            # 19 Operatividad. Se dejará sin info.
            datos_generales.write(row, 20, 'C3')
            # 21 Profesional de catastro. De dejará vacío.
            # 22 Supervisor. Dejar vacío.
            datos_generales.write(row, 23, descripcion_estructura(puente))
            # 24 Geometría. Todavía falta definir como generarla
            datos_generales.write(row, 25, 'Si')  # Está sin acento en la planilla
            # 26 Uso FF.CC. dejar vacío
            # 27 Uso permitido - Peatones. Dejar vacío.
            # 28 Uso permitido - Bicicletas. Dejar Vacío.
            # 29 Uso permitido - Otros. Dejar Vacío.
            # 30 Obstáculo salvado - S/I
            # 31 Obstáculo salvado - S/I
            # 32 Obstáculo salvado - Peatones. S/I
            # 33 Obstáculo salvado - Bicicletas. S/I
            datos_generales.write(row, 34, obstaculo_salvado_cauce(puente))
            # 35 Obstáculo salvado - Irregularidad del terreno. S/I
            # 36 Obstáculo salvado - Otros. S/I
            datos_generales.write(row, 37, puente['rol'])  # Ruta
            # 38 Coordenadas - Altitud. S/I
            datos_generales.write(row, 39, puente['latitud'])
            datos_generales.write(row, 40, puente['longitud'])
            datos_generales.write(row, 41, puente['anio_construccion'])
            # 42 Cronología - Nombre proyectista. S/I
            # 43 Cronología - Nombre constructor. S/I
            # 44 Cronología - Normativa técnica. S/I
            # 45 Cronología - Carga de diseño. S/I
            # 46 Cronología - Última rehab. (Año). S/I
            # 47 Cronología - Nombre proyectista. S/I
            # 48 Cronología - Nombre constructor. S/I
            # 49 Cronología - Normativa técnica. S/I
            # 50 Cronología - Carga de diseño. S/I
            # 51 Geometría - Trazado en planta. S/I
            datos_generales.write(row, 52, puente['esviaje'])
            # 53 Geometría - Trazado en elevación. S/I
            # 54 Geometría - k de proyecto. S/I
            datos_generales.write(row, 55, puente['cauce'])
            datos_generales.write(row, 56, puente['comentario_insp'])
            row += 1
        else:
            bms_excluidos.append(puente)
    # Mostramos los puentes sin equivalencia del catastro str-1245
    excel.close()
    resumen_filtros()


if __name__ == "__main__":
    db_bms = conectar_mysql('localhost', 'root', '11235813', 'uachbms')
    puentes = obtener_puentes_bms(db_bms)
    # print(puentes)
    excel = plantilla_excel()
    crear_excel(excel, puentes)